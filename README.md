# CaseWare SDK Blob Storage Demonstration
This repository exists to be a prototype of several methods to solve the issue of large blobs with the CaseWare SDK

# BitBucket piplines
There is a single BitBucket pipeline in the project which demonstrates three methods to solve hte large blob issue in the SDK.  The flow for this pipeline is

1. Clone the repository natively.  The `lfs: true` block tells BitBucket to hydrate any LFS blobs automatically
1. Download a separate file from an AWS S3 bucket
2. Zip the LFS image file with the S3 bucket file
3. Upload the zip to the BitBucket releases page for the repo

# Three ways to solve the problem of large blobs
## 1. Git LFS
LFS is Git solution for large file storage.  Large Git remote repositories like GitHub and BitBucket have native support for LFS.  For more information on Git LFS please see the documentation https://git-lfs.github.com/

The sample pipeline shows a very simple prototype for using Git LFS with BitBucket pipelines.  By adding `lfs: true` to the top of the pipeline, BitBucket autmatically (and natively) hydrates LFS blobs with no further action required by the pipeline developer. 

To confirm this test test, if `lfs: true` is removed from the pipeline, the zipped file uploaded to releases will contain a text pointer rather than the actual image.

There was some initial concern about a [security vulnerability posted by NIST](https://nvd.nist.gov/vuln/detail/CVE-2020-27955).  This vulnerability was (patched out a day before the security bulletin was released)[https://github.com/git-lfs/git-lfs/releases/tag/v2.12.1] in November 2020

## 2. Store the contents of the SE zip file in Git
The SE file is a compressed zip package of several text files.  If the individual text files in the archive are less than 100MB each, we could store the uncompressed files in Git instead of the zip.  When the package is generated for the nightly build, copy & paste the step from this project to zip the files to create the expected *.se file

## 3. AWS S3 Storage
Another option demonstrated in this project is to store the blob file AWS S3.  When the package is generated for the nightly build, copy and paste the step from this project to download the blob from S3.


# Supporting documentation
https://bitbucket.org/blog/git-lfs-now-available-bitbucket-pipelines

https://support.atlassian.com/bitbucket-cloud/docs/deploy-build-artifacts-to-bitbucket-downloads/

https://bitbucket.org/bitbucketpipelines/example-aws-elasticbeanstalk-deploy/src/master/bitbucket-pipelines.yml

https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/

