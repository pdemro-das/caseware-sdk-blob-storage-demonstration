###
# Code Soup Below
#
###

s3KId=$1
s3Secret=$2

timestamp=${timestamp-$(date -u +"%Y%m%dT%H%M%SZ")}
today=${today-$(date -u +"%Y%m%d")}

echo "timestamp: $timestamp"
echo "today: $today"

function create_canonical_request_file() {
    echo "Creating Canonical Request File"

    cat >s3-bucket.creq <<EOL
GET
/cpadotcom-s3.png

host:cw-bitbucket-pipeline-s3-poc.s3.us-east-2.amazonaws.com
x-amz-content-sha256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
x-amz-date:${timestamp}

host;x-amz-content-sha256;x-amz-date
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
EOL

    truncate -s -1 s3-bucket.creq #remove new line character at the end
}

function create_sts_file() {
    echo "Creating STS File"

    cat >s3-bucket.sts <<EOL
AWS4-HMAC-SHA256
${timestamp}
${today}/us-east-2/s3/aws4_request
${string_to_sign}
EOL

    truncate -s -1 s3-bucket.sts
}

create_canonical_request_file


string_to_sign=$(openssl dgst -sha256 s3-bucket.creq)
string_to_sign=${string_to_sign/SHA256(s3-bucket.creq)= /}
echo "string to sign: ${string_to_sign}";


create_sts_file

signing_key=$(/bin/bash signing_key.sh ${s3Secret} ${today} us-east-2 s3)

echo "signing key: ${signing_key}"



request_signature=$(openssl dgst -sha256 \
             -mac HMAC \
             -macopt hexkey:${signing_key} \
             s3-bucket.sts)
request_signature=${request_signature/HMAC-SHA256(s3-bucket.sts)= /}

echo "request signature: ${request_signature}"


curl -v https://cw-bitbucket-pipeline-s3-poc.s3.us-east-2.amazonaws.com/cpadotcom-s3.png \
     -H "Authorization: AWS4-HMAC-SHA256 \
         Credential=${s3KId}/${today}/us-east-2/s3/aws4_request, \
         SignedHeaders=host;x-amz-content-sha256;x-amz-date, \
         Signature=${request_signature}" \
     -H "x-amz-content-sha256: e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" \
     -H "x-amz-date: ${timestamp}" --output cpadotcom-s3.png
